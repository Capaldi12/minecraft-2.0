﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "OreType", menuName = "Minecraft 2/Ore Type", order = 1)]
public class OreType : ScriptableObject     //this class store information about ore lode ore other undeground features
{
    new public string name;
    public int blockID;
    public int maxHeight;
    public int minHeight;

    public float noiseOffset;
    public float noiseScale;
    public float noiseThreshold;
}
