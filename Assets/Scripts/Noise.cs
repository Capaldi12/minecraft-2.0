﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise   //used to generate 2D and 3D Perlin noise
{
    public static float seed;

    //now I use standard C# Perlin noise function
    //it has problems with whole numbers, which coordinates are
    //so 1.0f used to avoid this
    //I consider replacing this with other implementation
    static float GetNoise(float x, float y)
    {
        return Mathf.PerlinNoise(seed + x, seed + y);
    }
    public static float Get2D(Vector2 position, float offset, float scale)
    {
        return GetNoise((position.x + 0.1f) / Minecraft.ChunkW * scale + offset, 
                        (position.y + 0.1f) / Minecraft.ChunkW * scale + offset);
    }

    //3D noise can be easily calculated using intersections of 2D samples
    //this is basically "is there a block" function
    public static bool Get3D(Vector3 position, float offset, float scale, float threshold)
    {
        float x = (position.x + 0.1f) / Minecraft.ChunkW * scale + offset;
        float y = (position.y + 0.1f) / Minecraft.ChunkW * scale + offset;
        float z = (position.z + 0.1f) / Minecraft.ChunkW * scale + offset;

        float XY = GetNoise(x, y);
        float XZ = GetNoise(x, z);
        float YZ = GetNoise(y, z);

        float YX = GetNoise(y, x);
        float ZX = GetNoise(z, x);
        float ZY = GetNoise(z, y);

        return ((XY + XZ + YZ + YX + ZX + ZY) / 6f ) < threshold;
    }
}
