﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public int seed;
    public bool isRandom;

    public Transform player;
    public Vector3 spawnPoint;
    Coordinates playerChunk;
    Coordinates playerLastChunk;

    public Material textureMap;
    public BlockType[] blockTypes;
    public Biome[] biomes;

    //for big amounts of chunks map is better than array
    Dictionary<Coordinates, Chunk> chunks = new Dictionary<Coordinates, Chunk>();
    List<Coordinates> activeChunks = new List<Coordinates>();   //this way they can be easily deactivated later
    Queue<Coordinates> ChunksToCreate = new Queue<Coordinates>();   //to spread chunk creations between frames
    bool isApplyingModifications = false;   //to avoid reentering in ApplyModifications

    Queue<BlockMod> modifications = new Queue<BlockMod>();  //blocks to add after terrain generated
    List<Coordinates> chunksToUpdate = new List<Coordinates>();

    float updateTimer = 0;

    void Start()
    {
        if(isRandom)
        {
            seed = Mathf.FloorToInt(Random.Range(-100000, 100000));
        }
        Random.InitState(seed);
        Noise.seed = (float)seed;

        spawnPoint = new Vector3(Minecraft.WorldSizeB / 2f, //spawn in the middle of the world
                                 Minecraft.ChunkH / 2f + 30f, 
                                 Minecraft.WorldSizeB / 2f);
        GenerateWorld();
        player.position = spawnPoint;   //and put player to spawn
        playerLastChunk = GetChunkFromPos(player.position);
    }

    void Update()
    {
        updateTimer += Time.deltaTime;

        playerChunk = GetChunkFromPos(player.position);
        if(!playerLastChunk.Equals(playerChunk) || updateTimer > 3f)    //if player changed chunk or time has come
        {
            updateTimer = 0;
            playerLastChunk = playerChunk;
            UpdateWorld();
        }


        if(!isApplyingModifications && modifications.Count > 0) //adding trees and other(not yet) structures
            StartCoroutine("ApplyModifications");

        if(ChunksToCreate.Count > 0)    //generating requested chunks (one at a time)
            GenerateChunk();

        if(chunksToUpdate.Count > 0)    //updating chunks that need updating
            UpdateChunks();

    }

    void GenerateWorld()    //it just fills chunks array with generated chunks and generate structures
    {
        int middle = Minecraft.WorldSizeC / 2;
        for(int x = middle - Minecraft.ViewDistanceC; x <= middle + Minecraft.ViewDistanceC; x++)
        {
            for(int z = middle - Minecraft.ViewDistanceC; z <= middle + Minecraft.ViewDistanceC; z++)
            {   
                Coordinates c = new Coordinates(x, z);
                chunks.Add(c, new Chunk(this, c, true));
                activeChunks.Add(c);
            }
        }

        while(modifications.Count > 0)  //add structure blocks if there is
        {
            BlockMod mod = modifications.Dequeue();
            Coordinates c = GetChunkFromPos(mod.position);

            if(IsChunkInWorld(c))
            {
                if(!chunks.ContainsKey(c))  //create chunk if it is not
                {
                    chunks.Add(c, new Chunk(this, c, true));
                }

                if(!activeChunks.Contains(c))   //activate
                    activeChunks.Add(c);
                
                chunks[c].modifications.Enqueue(mod);   //enqueue modification

                if(!chunksToUpdate.Contains(c)) //request update
                    chunksToUpdate.Add(c);
                
            }
        }

        while(chunksToUpdate.Count > 0) //update all chunks
        {
            chunks[chunksToUpdate[0]].UpdateChunk();
            chunksToUpdate.RemoveAt(0);
        }
    }
    IEnumerator ApplyModifications()    //adding modification blocks to chunk (200 at a time)
    {   
        isApplyingModifications = true; //avoid reentering
        int count = 0;
        
        while(modifications.Count > 0)
        {
            BlockMod mod = modifications.Dequeue();
            Coordinates c = GetChunkFromPos(mod.position);

            if(IsChunkInWorld(c))
            {
                if(!chunks.ContainsKey(c))
                {
                    chunks.Add(c, new Chunk(this, c, true));    //create chunk if needed
                }

                if(!activeChunks.Contains(c))   //activate
                    activeChunks.Add(c);
                
                chunks[c].modifications.Enqueue(mod);   //enqueue modification

                if(!chunksToUpdate.Contains(c)) //request update
                    chunksToUpdate.Add(c);
                
            }

            count++;

            if(count > 200)
            {
                count = 0;
                yield return null;
            }
        }

        isApplyingModifications = false;
        yield break;
    }
    void GenerateChunk()    //generates chunnk first in queue for generation
    {
        Coordinates c = ChunksToCreate.Dequeue();
        if(!activeChunks.Contains(c))
            activeChunks.Add(c);
        chunks[c].GenerateChunk();
    }
    void UpdateChunks() //updates at most one chunk if there any to update
    {
        bool updated = false;   //one chunk updated
        int index = 0;

        while(!updated && index < chunksToUpdate.Count)
        {
            Coordinates c = chunksToUpdate[index];
            if(chunks[c].isBlockMap)    //update only if chunk is generated
            {
                chunks[c].UpdateChunk();
                chunksToUpdate.RemoveAt(index);

                updated = true;
            }
            else
            {
                index++;
            }
        }
    }

    public short GetNewBlock(Vector3 pos)  //there is where all generation is!
    {
        int y = Mathf.FloorToInt(pos.y);
        Vector2 pos2D = new Vector2(pos.x, pos.z);
        /* Boundaries */

        if(!IsBlockInWorld(pos))    //outside of the world
            return 0;

        if(y == 0)  //bedrock on bottom
            return 7;


        /* Biome */

        Biome biome = biomes[Noise.Get2D(pos2D, 100, 0.05f) <= 0.5f ? 0 : 1];

        /* Landscape */
        
        float heightOffset = biome.maxTerrainOffset *
                             Noise.Get2D(pos2D, 0, biome.terrainScale);
        int terrainLevel = biome.baseTerrainHeight + Mathf.FloorToInt(heightOffset);
        int block = 0;

        if(y > terrainLevel)   //air on top
            block = 0;

        else if(y == terrainLevel)
            block = biome.terrainBlolck;    //terrain top block
        else if(y >= terrainLevel - biome.subterrainHeight)
            block = biome.subterrainBlock;  //and blocks under it

        else
        {
            if(Noise.Get3D(pos, 0, 2, 0.55f))   //caves
                block = 1;
            else
                block = 0;
        }
        
        /* Ores */

        if(block == 1)
        {
            foreach(OreType ore in biome.ores)
            {
                if(y >= ore.minHeight && y <= ore.maxHeight)
                {
                    if(Noise.Get3D(pos, ore.noiseOffset, ore.noiseScale, ore.noiseThreshold))
                    {
                        block = ore.blockID;
                        break;
                    }
                }
            }
            
        }

        /* Trees */

        //trees only on surface and if there can be in current biome
        if(biome.TreeZoneThreshold > 0 && y == terrainLevel)
        {
            if(Noise.Get2D(pos2D, biome.TreeZoneOffset, biome.TreeZoneScale) < biome.TreeZoneThreshold)
            {
                //block = 4;

                if(Noise.Get2D(pos2D, biome.TreePlacementOffset, biome.TreePlacementScale) > biome.TreePlacementThreshold)
                {
                    Structure.Tree(pos, modifications, biome.minTreeSize, biome.maxTreeSize);
                }
            }
        }

        return (short)block;
    }
    public short GetBlock(Vector3 pos)  //get existing block, if there is, otherwise - get new one
    {
        if(!IsBlockInWorld(pos))
            return -1;

        Coordinates c = new Coordinates(pos);
        Chunk chunk;
        if(chunks.TryGetValue(c, out chunk) && chunk.isBlockMap)
            return chunk.GetBlock(pos);

        return GetNewBlock(pos);
    }

    void UpdateWorld()  //there we check if chunks need to be generated, activated or deactivated
    {   
        List<Coordinates> previouslyActive = new List<Coordinates>(activeChunks);   //to disable chunks too far from player

        //check all the chunks in ViewDistance from player
        for(int x = playerChunk.x - Minecraft.ViewDistanceC; x <= playerChunk.x + Minecraft.ViewDistanceC; x++)
        {
            for(int z = playerChunk.z - Minecraft.ViewDistanceC; z <= playerChunk.z + Minecraft.ViewDistanceC; z++)
            {
                Coordinates c = new Coordinates(x, z);
                if(IsChunkInWorld(c))
                {
                    Chunk chunk;
                    if(!chunks.TryGetValue(c, out chunk))
                    {   //generate chunk if it is not
                        chunks.Add(c, new Chunk(this, c, false));    
                        ChunksToCreate.Enqueue(c);  
                        activeChunks.Add(c);
                    }
                    else if(!chunk.isActive)    
                    {   //activate and update inactive chunk
                        chunk.isActive = true;  
                        activeChunks.Add(c);
                        chunksToUpdate.Add(c);
                    }
                    
                    //remove chunks from this array if they are still active
                    for(int i = 0; i < previouslyActive.Count; i++)
                    {
                        if(previouslyActive[i].Equals(c))
                            previouslyActive.RemoveAt(i);
                    }
                }
            }
        }

        //and here we deactivate chunks that are not in the view distance(was active and not active now)
        foreach(Coordinates c in previouslyActive)  
        {
            if(chunks[c].isBlockMap)    //should not deactivate chunk, that is not generated yet, or it'll stay active till next activation
            {
                chunks[c].isActive = false;
                activeChunks.Remove(c);
            }
        }
    }

    Coordinates GetChunkFromPos(Vector3 pos)    //get coordinates of chunk where given position is
    {
        int x = Mathf.FloorToInt(pos.x / Minecraft.ChunkW);
        int z = Mathf.FloorToInt(pos.z / Minecraft.ChunkW);

        return new Coordinates(x, z);
    }
    public Chunk FindChunk(Vector3 pos) //get refference to chunk where given position is
    {
        int x = Mathf.FloorToInt(pos.x / Minecraft.ChunkW);
        int z = Mathf.FloorToInt(pos.z / Minecraft.ChunkW);

        return chunks[new Coordinates(x, z)];
    }

    public bool IsChunkInWorld(Coordinates cd)
    {   //I'll possibly change this so 0, 0 will be center of the world and not the corner
        return !(cd.x < 0 || cd.x >= Minecraft.WorldSizeC || cd.z < 0 || cd.z >= Minecraft.WorldSizeC);
    }
    public bool IsBlockInWorld(Vector3 pos)
    {
        return !(pos.x < 0 || pos.x >= Minecraft.WorldSizeB || 
                 pos.y < 0 || pos.y >= Minecraft.ChunkH || 
                 pos.z < 0 || pos.z >= Minecraft.WorldSizeB);
    }
}

[System.Serializable]   //for unity to be able to display this class properly
public class BlockType  //this class stores information about block
{
    [Header("Properties")]
    public string name;
    public bool isSolid;    //not solid block is only air right now, but there will be more (water, lava, cobweb etc.)
    public bool isOpaque;   

    [Header("Textures")]    //texture indices for each face
    public int backTexture;
    public int frontTexture;
    public int bottomTexture;
    public int topTexture;
    public int leftTexture;
    public int rightTexture;

    //textures split this way, so it's easy to set them from Inspector
    //but it requires get metod
    public int GetTextureIndex(int faceIndex)
    {
        switch(faceIndex)
        {
            case 0:
                return backTexture;
            case 1:
                return frontTexture;
            case 2:
                return bottomTexture;
            case 3:
                return topTexture;
            case 4:
                return leftTexture;
            case 5:
                return rightTexture;
            default:
                Debug.Log("getTextureInex: Incorrect faceIndex argument: " + faceIndex);
                return 0;
        }
    }
}

public class BlockMod   //this class stores information about modification to be applied(block to add in the world)
{
    public Vector3 position;
    public short id;

    public BlockMod()
    {
        position = new Vector3();
        id = 0;
    }
    public BlockMod(Vector3 pos, short _id)
    {
        position = pos;
        id = _id;
    }
}