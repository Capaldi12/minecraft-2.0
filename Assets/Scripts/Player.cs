﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour //player controller class
{
    [SerializeField] float mouseSensivity = 5;
    [SerializeField] float speed = 30f;
    [SerializeField] float sprintSpeed = 60f;
    //[SerializeField] float maxSpeed = 100f;

    Transform cam;

    float horizontal;
    float vertical;
    float mouseHorizontal;
    float mouseVertical;

    Vector3 direction;

    void Start() 
    {
        cam = GameObject.Find("Camera").transform;
        Cursor.lockState = CursorLockMode.Locked;   //keep cursor in center of the screen
    }

    void Update()
    {
        Cursor.visible = false;

        bool sprint = getInput();
    
        //rotation
        transform.Rotate(Vector3.up * mouseHorizontal * mouseSensivity); //horizontal rotation

        //vertical rotation with clamp on top-most and bottom-most points
        float clampedY = ClampAngle(cam.localEulerAngles.x + mouseVertical * mouseSensivity);
        cam.localEulerAngles = new Vector3(clampedY, cam.localEulerAngles.y, cam.localEulerAngles.z);

        //movement
        //movement direcion vector
        direction = new Vector3(
        horizontal, //sideways movement is simply horizontal axis value
        Mathf.Sin(clampedY * Mathf.PI / 180f) * -vertical,  //forward movement must be scaled with sin and cos
        Mathf.Cos(clampedY * Mathf.PI / 180f) * vertical    // of camera angle from horizontal plane
        );
        
        direction *= Time.deltaTime;    //to have consistent movement

        if(sprint)  //scale with speed
            direction *= sprintSpeed;
        else
            direction *= speed;

        transform.Translate(direction); //apply movement
    }

    bool getInput()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        mouseHorizontal = Input.GetAxis("Mouse X");
        mouseVertical = Input.GetAxis("Mouse Y");

        return Input.GetButton("Sprint");
    }

    float ClampAngle(float a)
    {
        //eulerAngles seem to return only positive 0 to 360 values, but accept -180 to 180 values as well
        //so I get value in this span if it is not
        if(a > 180) a -= 360;   
        return Mathf.Clamp(a, -90f, 90f);
    }
}
